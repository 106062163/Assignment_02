# Software Studio 2019 Spring Assignment 2
Website: https://106062163.gitlab.io/Assignment_02/
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms :
* <h4> Except level 1, player need to kill the boss then go to next level</h4>
* <h4> In level 1, just have 5 monster need to kill</h4>
* <h4>Level display in top of window :  <img src="/report/level.PNG" alt=""></h4>
* <h4>For using skill, need skill energy, get energy by attacking monster, energy bar displaied in left side</h4>
* <h4>when skill energy is full, then can press Space key to use it
<img src="/report/skill.gif" alt="" style="display:block"></h4>
* <h4>Skill energy also will cost by shield of P2(defend bullet)</h4>
<br>
2. Animations :
* <h4> Player move left and right with animation : <img src="/report/player.gif" alt=""></h4>
3. Particle Systems : 
* <h4> Appear particle when player's bullet hit monster, : <img src="/report/particle.gif" alt=""></h4>
* <h4> also happen when monster's bullet hit player, : <img src="/report/playPar.gif" alt=""></h4>

4. Sound effects : 
* <h4>There is a background music auto play and keep looping in the game.</h4>
* <h4>Also when monster die, it will play a explosion sound.</h4>
* <h4>available to change the sound volume after <span style='color:red'>pressing the stop button</span>, and press + or - to change volume.</h4>
<h4><img src="/report/sound.gif" alt=""></h4>

5. Leaderboard : 
* <h4>After lose, player can input his name, and save the name with score to firebase,</h4>
* <h4>Then, player can see what rank he was in leaderboard, <span style='color:red'>press Left and Right key</span> to check other people's score</h4>
<h4><img src="/report/firebase.gif" alt=""></h4>

# Bonus Functions Description : 
1. Multi-player game - offline
* <h4>At first can choose Solo(one player) or Duo(two player) to play<img src="/report/playNum.PNG" alt=""></h4>
* <h4>Player one: up down left right to move, space to use skill</h4>
* <h4>Player Two: W A S D to move, no skill, use blade to damage monster and have shield to resist bullet by costing energy.</h4>
* <h4>Only player one can use item</h4>
2. Enhanced items : 
* <h4>Item 1: After overlap item 1, player one can shoot missile and it will cost a huge damage.</h4>
<h4><img src="/report/item1.gif" alt=""></h4>

* <h4>Item 2: After overlap item 2, the missile(from item 1) will auto automatic aiming random monster.</h4>
* <h4>Item 3: After overlap item 3, a little helper will appear and follow player one, shooting bullet.</h4>
<h4><img src="/report/item23.gif" alt=""></h4>

3. Unique boss : 
* <h4>After level 1, all level will appear a boss in the last</h4>
* <h4>Boss's health and attack frequency will increase in higher level</h4>
* <h4>Here is how the boss look and attack way</h4>
<h4><img src="/report/boss.gif" alt=""></h4>

## End, thank you!!
