var OneP
var TwoP
var StartState = {
    preload: function () {
        
    },
    create: function () {
        if (!game.device.desktop) {
            // Set the type of scaling to 'show all'
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            // Set the min and max width/height of the game
            game.scale.setMinMax(game.width / 2, game.height / 2,
                game.width * 2, game.height * 2);
            // Center the game on the screen
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            // Add a blue color to the page to hide white borders
            document.body.style.backgroundColor = '#3498db';
        }
        /* game.stage.backgroundColor = '#3498db'; */
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        var nameLabel = game.add.text(game.world.centerX, -50,
            'Radien', {
                font: '70px Arial',
                fill: '#ffffff'
            });
        nameLabel.anchor.setTo(0.5, 0.5)
        var tween = game.add.tween(nameLabel);

        tween.to({
            x: game.world.centerX,
            y: 120
        }, 800);
        tween.start();


        var startKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        startKey.onDown.add(this.start, this);

        OneP = game.add.button(200, 300, '1pBut', this.start)
        TwoP = game.add.button(500, 300, '2pBut', this.start)
        OneP.name = "one"
        TwoP.name = "two"
        var tween2 = game.add.tween(OneP)
        tween2.to({
            x: 200,
            y: 300
        })
        tween2.start()
        var tween3 = game.add.tween(TwoP)
        tween3.to({
            x: 500,
            y: 300
        })
        tween3.start()
        /* game.state.start('load') */
        /* game.global.playerNum = "two"
        game.state.start('load') */
    },
    update: function () {
        
    },
    start: function (b) {
        game.global.playerNum = b.name
        game.state.start('load')
    }

}