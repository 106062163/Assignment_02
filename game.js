var game = new Phaser.Game(850, 600, Phaser.AUTO,'canvas');

game.global = {
    score: 0,
    level: 1,
    musicVolume: 0.8,
    playerNum: "one"
}

game.state.add('boot',BootState)
game.state.add('start', StartState);
game.state.add('load', LoadState);
game.state.add('play', PlayState);
game.state.add('over', OverState);
game.state.add('rank',RankState)
game.state.start('boot')
/* game.state.start('start') */
